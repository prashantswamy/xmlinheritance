package com.infosys.accelerate;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author prashant.swamy
 *
 */
@XmlRootElement
public class Customer {
	private Object contactInfo;

	public Object getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(Object contactInfo) {
		this.contactInfo = contactInfo;
	}
}
