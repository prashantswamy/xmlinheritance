package com.infosys.accelerate.client;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;

import com.infosys.accelerate.Address;
import com.infosys.accelerate.Customer;
import com.infosys.accelerate.PhoneNumber;

/**
 * @author prashant.swamy
 *
 */
public class Demo {

	/**
	 * @param args
	 * @throws JAXBException
	 */
	public static void main(String[] args) throws JAXBException {
		Customer customer = getAddress();
		printCustomer(customer);

		System.out.println("\n");
		customer = getPhoneNumber();
		printCustomer(customer);
	}

	/**
	 * @param customer
	 * @throws JAXBException
	 * @throws PropertyException
	 */
	private static void printCustomer(Customer customer) throws JAXBException, PropertyException {
		JAXBContext jc = JAXBContext.newInstance(Customer.class, Address.class, PhoneNumber.class);

		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(customer, System.out);
	}

	/**
	 * @param customer
	 */
	private static Customer getPhoneNumber() {
		Customer customer = new Customer();
		PhoneNumber phoneNumber = new PhoneNumber();
		phoneNumber.setPhoneNumber("7588525491");
		customer.setContactInfo(phoneNumber);
		return customer;
	}

	/**
	 * @param customer
	 */
	private static Customer getAddress() {
		Customer customer = new Customer();
		Address address = new Address();
		address.setStreet("1 A Street");
		customer.setContactInfo(address);
		return customer;
	}
}
