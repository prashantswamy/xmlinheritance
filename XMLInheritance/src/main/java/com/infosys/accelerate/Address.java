/**
 * 
 */
package com.infosys.accelerate;

/**
 * @author prashant.swamy
 *
 */
public class Address extends ContactInfo {
	private String street;

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}
}
