package com.infosys.accelerate;

/**
 * @author prashant.swamy
 *
 */
public class PhoneNumber extends ContactInfo {
	private String phoneNumber;

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
